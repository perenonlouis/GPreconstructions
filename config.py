# This class defines global variables that will not change

import numpy as np
import sys, os


def set_defaults():
    global defaults
    defaults = {

        ################
        ### Sampling ###
        ################
        "cobaya_MH_chain_burn": 0.5,
        "cobaya_MH_max_tries": 200000,
        "cobaya_MH_Rminus1_stop": 0.001,
        "cobaya_polychord_nlive": 2000,

        #######################
        ### Marginalisation ###
        #######################
        "chain_burn_for_marginalisation": 0.5,
        "chain_thin_for_marginalisation": 20,
        "mc_gen_for_marginalisation": 1000,
    }


def set_general_options(general_options,data,GPinfo):
    global options
    global specs
    options = general_options

    specs = {'observables': data['names'],
             'ylabels': data['ylabels'],
             'xlabels': data['xlabels'],
             'method': GPinfo['method']}



    # Here we can place checks. Like if you want marginalization
    # but select False to the sample_hyperparameter options
    # the latter will be changed to make the code work

    # options['path_to_Polychord'] = "/home/matteo/Codes/cobaya_stuff/packages"
    # options['path_to_Polychord'] = "/home/louis/WORK/CODES/Polychord"


def set_covfunc(cfg,GPinfo):

    if GPinfo['method'] == 'single task':

        covfunc = []
        for key in cfg.specs['observables']:

            covobs = {"name": GPinfo[key]['kernel'],
                      "hyp_names": [par for par in GPinfo[key]["hyperpars"]],
                      "hyp_values": [0.1]*len(GPinfo[key]["hyperpars"]), #MM: is this needed? 
                      "mean_priors": {'mean': GPinfo[key]['mean']}}

            covfunc.append(covobs)
    elif GPinfo['method'] == 'multi task':
        covfunc = {}
        kernels = []

        for ind1,obs1 in enumerate(cfg.specs['observables']):

            tempvec = []
            for ind2,obs2 in enumerate(cfg.specs['observables']):
                if ind1 == ind2:
                    tempvec.append(GPinfo[obs1]['kernel'])
                else:
                    tempvec.append('Conv')

            kernels.append(tempvec)

        covfunc['name'] = kernels
        covfunc['hyp_names'] = []
        covfunc['hyp_values'] = []
        meanvec = []
        for obs in cfg.specs['observables']:
            covfunc['hyp_names'].extend([par for par in GPinfo[obs]["hyperpars"]])
            covfunc['hyp_values'].extend([0.1]*len(GPinfo[obs]["hyperpars"]))
            meanvec.append(GPinfo[obs]['mean'])

        covfunc['mean_priors'] = {'mean': meanvec}


    else:
        raise ValueError(
                 "/!\ Unknown method {}. /!\ ".format(GPinfo['method']))


    return covfunc

def set_sampling(cfg,GPinfo):

    if GPinfo['method'] == 'single task':
        sampling_info = []

        for obs in cfg.specs['observables']:

            sampobs = {"likelihood": cfg.options['likelihood']}
            sampobs['hyp_ranges'] = {}


            for ind,key in enumerate(GPinfo[obs]['hyperpars']):
                sampobs['hyp_ranges'][key] = GPinfo[obs]['hyperpars'][key]['range']

            sampobs['hyp_labels'] = [key+obs for key in GPinfo[obs]['hyperpars']]
            sampobs['sampling_func'] = [GPinfo[obs]['hyperpars'][key]['sampling_func'] for key in GPinfo[obs]['hyperpars']]

            sampobs["sampler"]   = cfg.options['sampler']
            sampobs["minimizer"] = cfg.options['minimizer']

            sampling_info.append(sampobs)

    elif GPinfo['method'] == 'multi task':
        sampling_info = {"likelihood": cfg.options['likelihood'],
                         "sampler": cfg.options['sampler'],
                         "minimizer": cfg.options['minimizer']}

        sampling_info['hyp_ranges']    = {}
        sampling_info['hyp_labels']    = []
        sampling_info['sampling_func'] = []

        for obs in cfg.specs['observables']:
            for par in GPinfo[obs]['hyperpars']:
                sampling_info['hyp_ranges'][par] = GPinfo[obs]['hyperpars'][par]['range']

            sampling_info['hyp_labels'].extend([key for key in GPinfo[obs]['hyperpars']])
            sampling_info['sampling_func'].extend([GPinfo[obs]['hyperpars'][key]['sampling_func'] for key in GPinfo[obs]['hyperpars']])

    else:
        raise ValueError(
                 "/!\ Unknown method {}. /!\ ".format(GPinfo['method']))


    return sampling_info

class HiddenPrints:
    def __enter__(self):
        self._original_stdout = sys.stdout
        sys.stdout = open(os.devnull, 'w')

    def __exit__(self, exc_type, exc_val, exc_tb):
        sys.stdout.close()
        sys.stdout = self._original_stdout
