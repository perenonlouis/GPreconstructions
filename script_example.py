import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from master import *


general_options = {'name_run': 'results/test',
                   'method': 'optimization',  # marginalisation is the alternative
                   'sample_hyperparameters': True,
                   'minimize_hyperparameters': True,
                   'path_to_Polychord': '/home/louis/WORK/CODES/Polychord',
                   'print_chains': True,
                   'print_plots': True}


# z = np.linspace(0,2,100)
# H = 67.*np.sqrt(0.3*(1+z)**3+0.7)
# err = H*0.1

# fake = np.array([[i, j, k] for i, j, k in zip(z,H,err)])

# data = {"xyerr": fake,
#         "cov": np.diag(err),
#         "xlabel": r'$z$',
#         "ylabel": r'$H(z)$'}

data = {
    "xyerr": np.loadtxt("../data/data_gold_fsig.txt"),
    "cov": np.loadtxt("../data/data_gold_fsig_cov.txt"),
    'xlabel': r'$z$',
    'ylabel': r'$f\sigma_8$'
}

pars = np.polyfit(data['xyerr'][:, 0], data['xyerr'][:, 1], 3)


covfunc = {
    "name": "SE",
    "hyp_names": ["sigma", "length"],
    "hyp_values": [0.1, 0.1],
    # pars[0]*x**3+pars[1]*x**2+pars[2]*x+pars[3] }
    "mean_priors": {'mean': lambda x: 0. * x}
}

sampling_info = {
    "likelihood": "logmarginal",
    "hyp_ranges":{'sigma': [-2,1],
                  'length': [-2,4]},
    "hyp_labels": [r"log_{10}\sigma", r"log_{10}l"],
    "sampling_func": lambda x: 10**np.array(x),
    "sampler": "cobaya_polychord",
    "minimizer": "cobaya_bobyqa"
}


# sampling_info = {
#     "likelihood": "logmarginal",
#     "hyp_ranges": [
#         [0, 10],
#         [0, 100],
#     ],
#     "sampler": "cobaya_MH",
# }


fullcode = MainEngine(general_options, data, covfunc, sampling_info)
