import numpy as np
from matplotlib.pyplot import plot

my_fsig_x_pred_1 = np.linspace(0.0001, 1, 500)
my_fsig_x_pred_2 = np.linspace(0.0001, 2, 500)

my_fsig_colors = {
    "gp": "purple",
    "ref": "black",
    "data": "black",
}

my_fsig_labels = {
    "x": r"$z$",
    "y": r"$f\sigma_8$",
    "ref": "Planck 2018 clik best fit",
}
