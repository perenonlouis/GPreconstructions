import numpy as np
import matplotlib.pyplot as plt
from getdist.mcsamples import MCSamplesFromCobaya
import getdist.plots as gdplt

# ----------------------------
import config

from .my_fsigma8plots import *
#import my_lcdm
# ----------------------------


def plot_contours(results, bestfit):

    paramvec = [key for key in results.info['params']]
    limits = {}
    for par in paramvec:
        limits[par] = [results.info['params'][par]['prior']
                       ['min'], results.info['params'][par]['prior']['max']]

    # paramvec.append('chi2')

    gd_sample = MCSamplesFromCobaya(
        results.info,
        results.sampling_results["sample"],
        ignore_rows=config.options['burn_in']
    )
    gdplot = gdplt.get_subplot_plotter(
        subplot_size=1, width_inch=12, scaling=False)

    gdplot.settings.legend_fontsize = 22
    gdplot.settings.axes_fontsize = 26
    gdplot.settings.lab_fontsize = 30
    # gdplot.settings.x_label_rotation = 45

    gdplot.triangle_plot(
        gd_sample,
        paramvec,
        filled=True,
        contour_colors=["darkred"],
        param_limits=limits,
        markers=bestfit,
        marker_args={'lw': 2, 'color': 'black'}
    )
    if config.options['print_plots']:
        gdplot.fig.savefig(
            config.options['name_run'] + '_triplot.pdf', dpi=300, bbox_inches='tight')
    else:
        gdplot.fig.show()


def plot_reconstruction(gpinput, recondict, fiducial):

    for ind,obs in enumerate(config.specs['observables']):

         recon    = recondict[obs]


         plt.figure(figsize=(10, 8))
         plt.fill_between(recon['x'], recon['down2'], recon['up2'],
                          color='darkred', alpha=0.2)
         plt.fill_between(recon['x'], recon['down1'], recon['up1'],
                          color='darkred', alpha=0.4)
         plt.plot(recon['x'], recon['mean'], color='darkred', lw=2, alpha=1,
                  label='GP reconstruction (' + config.options['method'] + ')')

         if config.specs['method'] == 'single task':
            gp = gpinput[obs]
            plt.errorbar(gp.data_x, gp.data_y, yerr=gp.data_err,
                         fmt=".k", capsize=0, label="Data")
         elif config.specs['method'] == 'multi task':
            gp = gpinput

            plt.errorbar(gp.data_x[ind], gp.data_y[ind], yerr=gp.data_err[ind],
                         fmt=".k", capsize=0, label="Data")


         if fiducial is not None:
            plt.plot(recon['x'],fiducial[obs](recon['x']),color='black',label='Fiducial')

       
         if 'ylabel' in recon:
             ylab = recon['ylabel']
         else:
             ylab = r'$y$'

         if 'ylabel' in recon:
             xlab = recon['xlabel']
         else:
             xlab = r'$x$'

         if obs in ['sigma8','fsigma8']:
             plt.legend(loc='lower left', fontsize=16)
         else:
             plt.legend(loc='upper right', fontsize=16)
         plt.xlabel(xlab, fontsize=24)
         plt.ylabel(ylab, fontsize=24)
         plt.xticks(fontsize=18)
         plt.yticks(fontsize=18)

         if config.options['print_plots']:
             if config.options["method"] == 'optimization':
                plt.savefig(config.options['name_run'] +'_'+
                            obs+'_reconstruction_optimised_'+config.specs['method']+'.pdf', dpi=300, bbox_inches='tight')
             else:
                plt.savefig(config.options['name_run'] +'_'+
                            obs+'_reconstruction_marginalised_'+config.specs['method']+'.pdf', dpi=300, bbox_inches='tight')
         else:
             plt.show()
