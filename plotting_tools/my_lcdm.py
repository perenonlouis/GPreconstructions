import numpy as np
from scipy import misc
from scipy import interpolate
import classy
import hi_classy


def growth_lcdm(x_pred, which='planck2018'):
    if which == 'planck2018':
        params = {
            'output': 'mPk',
            'P_k_max_h/Mpc': 4.,
            'z_max_pk': max(x_pred),

            'omega_b': 0.022383,
            'omega_cdm': 0.12011,
            'h': 67.32 / 100,
            'tau_reio': 0.0543,
            'ln10^{10}A_s': 3.0448,
            'n_s': 0.96605,
        }

    cosmo = classy.Class()
    cosmo.set(params)
    cosmo.compute()

    def fsigma8(z, engine):
        return -(1 + z) * misc.derivative(
            lambda z: engine.sigma(8. / engine.h(), z), x0=z, dx=1e-4
        )

    f = np.vectorize(lambda z: fsigma8(z, cosmo) /
                     cosmo.sigma(8. / cosmo.h(), z))
    sig = np.vectorize(lambda z: cosmo.sigma(8. / cosmo.h(), z))
    fsig = np.vectorize(lambda z: fsigma8(z, cosmo))

    return {
        "f": f(x_pred).tolist(),
        "sig": sig(x_pred).tolist(),
        "fsig": fsig(x_pred).tolist(),
    }


def growth_horn(x_pred, which='planck2018'):
    if which == 'planck2018':
        params = {
            'output': 'mPk',
            'T_cmb': 2.7255,
            'P_k_max_h/Mpc': 4.,
            'z_max_pk': max(x_pred),

            'N_ur': 0.00641,
            'N_ncdm': 3,
            'expansion_model': 'wowa',
            'gravity_model': 'propto_omega',

            'omega_b': 2.263504963299513284e-02,
            'omega_cdm': 1.178082580466783130e-01,
            'h': 6.835246553917839663e+01 / 100,
            'tau_reio': 5.089264440195728767e-02,
            'ln10^{10}A_s': 3.032410759651294807e+00,
            'n_s': 9.721155998303103729e-01,
            'm_ncdm': str(2.442115210014903989e-02 / 3) + ',' + str(2.442115210014903989e-02 / 3) + ',' + str(2.442115210014903989e-02 / 3),

            'Omega_Lambda': 0.,
            'Omega_fld': 0.,
            'Omega_smg': -1.,
            'expansion_smg':  '0.5,' + str(-9.595433704639984018e-01) + ',' + str(-9.482432370591738213e-02),
            'parameters_smg':  str(5.) + ',' + str(1.996434873593840642e+00) + ',' + str(3.082825399555531032e+00) + ',0.,1.',
        }

    cosmo = hi_classy.Class()
    cosmo.set(params)
    cosmo.compute()

    def fsigma8(z, engine):
        return -(1 + z) * misc.derivative(
            lambda z: engine.sigma(8. / engine.h(), z), x0=z, dx=1e-4
        )

    f = np.vectorize(lambda z: fsigma8(z, cosmo) /
                     cosmo.sigma(8. / cosmo.h(), z))
    sig = np.vectorize(lambda z: cosmo.sigma(8. / cosmo.h(), z))
    fsig = np.vectorize(lambda z: fsigma8(z, cosmo))

    return {
        "f": f(x_pred).tolist(),
        "sig": sig(x_pred).tolist(),
        "fsig": fsig(x_pred).tolist(),
    }
