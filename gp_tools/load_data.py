import numpy as np


class Load:
    """"This class loads the data dictionary into the class variables"""

    def __init__(self, data):

        __dic_keys = list(data.keys())
        if "x" in __dic_keys:
            self.data_x = data["x"]

        if "y" in __dic_keys:
            self.data_y = data["y"]

        if "xy" in __dic_keys:
            try:
                self.data_x, self.data_y = data["xy"]
            except:
                self.data_x, self.data_y = data["xy"].T

        if "xyerr" in __dic_keys:
            try:
                self.data_x, self.data_y, self.data_err = data["xyerr"]
            except:
                self.data_x, self.data_y, self.data_err = data["xyerr"].T
            self.data_cov = np.diag(self.data_err**2)
            print(np.shape(self.data_cov))
            self.data_invcov = np.linalg.inv(self.data_cov)
            print(np.shape(self.data_invcov))

        if "cov" in __dic_keys:
            self.data_cov = data["cov"]
            #MM: ugly fix
            ####################################
            if isinstance(self.data_x[0], list):
                self.data_err = []
                pos = 0
                for obs in range(len(self.data_x)):
                    single_err = [np.sqrt(self.data_cov[ind,ind]) for ind in range(pos,pos+len(self.data_x[obs]))]
                    self.data_err.append(single_err)
                    pos += len(self.data_x[obs])
            else:
                self.data_err = np.sqrt(np.diag(self.data_cov))
            ####################################

            self.data_invcov = np.linalg.inv(self.data_cov)
        elif "err" in __dic_keys:
            self.data_cov = np.diag(data["err"]**2)
            self.data_err = data["err"]
            self.data_invcov = np.linalg.inv(self.data_cov)

        # if np.any(__dic_keys) not in np.any("xyerr", "cov", "err", "invcov"):
        #
        #     self.data_cov = 0
        #     self.data_err = 0

        if "invcov" in __dic_keys:
            self.data_invcov = data["invcov"]
        else:
            self.data_invcov = np.linalg.inv(self.data_cov)

        if "invcov" in __dic_keys and "cov" not in __dic_keys:
            self.data_cov = np.linalg.inv(self.data_invcov)
            self.data_err = np.sqrt(np.diag(self.data_cov))

        if "detcov" in __dic_keys:
            self.data_detcov = data["detcov"]
        else:
            self.data_detcov = np.linalg.det(self.data_cov)

        self.data_is_single_task = isinstance(self.data_x[0], float)
        if self.data_is_single_task:
            self.data_n = len(self.data_x)
        else:
            self.data_n = np.size(np.block(self.data_x))

        if all([key in data for key in ["xlabel", "ylabel"]]):
            self.data_xlabel = data['xlabel']
            self.data_ylabel = data['ylabel']
        else:
            self.data_xlabel = r'$x$'
            self.data_ylabel = r'$y$'
