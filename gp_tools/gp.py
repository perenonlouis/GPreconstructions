import numpy as np

from .load_covfunc import Load as load_covfunc
from .load_data import Load as load_data

"""This Gaussian Process regression code has been inspired by :
    - The Bible :
        Gaussian Processes for Machine Learning
        C. E. Rasmussen & C. K. I. Williams, Gaussian Processes for Machine Learning, the MIT Press, 2006,
        ISBN 026218253X. c 2006 Massachusetts Institute of Technology. www.GaussianProcess.org/gpml

    - GaPP code: arXiv:1204.2832

    - arXiv:1912.04325
"""


def QR_inverse(mat):
    Q, R = np.linalg.qr(mat)
    return np.dot(Q, np.linalg.inv(R.T))


class Engine(load_data, load_covfunc):
    """The GP engine class"""

    def __init__(self, data, covfunc):
        load_data.__init__(self, data)
        load_covfunc.__init__(self, covfunc)

        # --- Check that the default hyp values give K+C matrix invertible
        self.set_hyp_values(self.default_hyp_values)

        # --- Set mean priors
        if "mean_priors" in covfunc.keys():
            self.set_mean_priors(covfunc["mean_priors"])
        else:
            if self.data_is_single_task:
                def mean_prior(x):
                    return np.full(len(self.data_x), 0)

                def dmean_prior(x):
                    return np.full(len(self.data_x), 0)
            else:
                mean = []
                dmean = []
                for i in range(np.shape(self.data_x)[0]):
                    mean = np.hstack((
                        mean,
                        np.full(len(self.data_x), 0),
                    ))

                    dmean = np.hstack((
                        dmean,
                        np.full(len(self.data_x), 0)
                    ))

            self.set_mean_priors({
                "mean": mean_prior,
                "dmean": dmean_prior,
            })

    ####################################
    ### Checkers / Setters / Getters ###
    ####################################

    def _check_hyp_values(self):
        """Checker that the K+C matrix is invertible"""
        self.cov_KC = self.covfunc.kernel(self.data_x, self.data_x) \
            + self.data_cov
        self.invcov_KC = QR_inverse(self.cov_KC)
        self.invcov_KC_test = np.max(
            np.abs(np.dot(self.cov_KC, self.invcov_KC) - np.eye(self.data_n))
        )
        if self.invcov_KC_test > 1e-8:
            raise ValueError(
                "Inversion of K+C not precise enough: \
                max(abs(KC*KC^-Id)) > 1e-8"
            )

    def set_hyp_values(self, p):
        """Setter of hyperparameter values"""
        self.hyp_values = p
        self.covfunc.set_hyp_values(p)
        self._check_hyp_values()

    def get_hyp_values(self):
        """Getter of hyperparameter values"""
        return self.hyp_values

    def set_mean_priors(self, priors):
        """Setter of the mean priors on the reconstruction"""
        if "mean" in list(priors.keys()):
            self.prior_mean = priors["mean"]

        if "dmean" in list(priors.keys()):
            self.prior_dmean = priors["dmean"]

        if self.data_is_single_task:
            self.Y = np.block(self.data_y) - self.prior_mean(self.data_x)
        else:
            self.Y = np.block(self.data_y) - np.block(
                [self.prior_mean[i](x) for i, x in enumerate(self.data_x)]
            )

        self.YT = np.transpose(self.Y)

    ###################
    ### Predictions ###
    ###################

    def predict(self, x_star, predict_cov=True):
        """Computes the mean and covariance"""
        self.cov_XstarX = self.covfunc.kernel(x_star, self.data_x)
        if self.data_is_single_task:
            mean = self.prior_mean(x_star) + np.linalg.multi_dot(
                [self.cov_XstarX, self.invcov_KC, self.Y]
            )
        else:
            mean = np.block(
                [self.prior_mean[i](x) for i, x in enumerate(x_star)]
            ) + np.linalg.multi_dot(
                [self.cov_XstarX, self.invcov_KC, self.Y]
            )
        if predict_cov:
            cov = self.covfunc.kernel(x_star, x_star) \
                - np.linalg.multi_dot([
                    self.cov_XstarX,
                    self.invcov_KC,
                    np.transpose(self.cov_XstarX)
                ])
            return mean, cov
        else:
            return mean

    def log_marginal_likelihood(self):
        """Computes log marginal likelihood"""
        res = np.linalg.multi_dot([self.YT, self.invcov_KC, self.Y])
        res += np.linalg.slogdet(self.cov_KC)[1]
        res += self.data_n * np.log(2 * np.pi)
        return -.5 * res

    def log_marginal_likelihood_cholesky(self):
        """Computes log marginal likelihood unsing Cholesky decomposition"""
        L = np.linalg.cholesky(self.cov_KC)
        alpha = np.linalg.solve(np.transpose(L), np.linalg.solve(L, self.Y))
        return (
            - .5 * np.dot(np.transpose(self.Y), alpha)
            - np.sum(np.log(np.diagonal(L)))
            - .5 * self.data_n * np.log(2 * np.pi)
        )

    def grad_log_marginal_likelihood(self):
        """Computes the gradient of the log marginal likelihood"""
        grad_kernel = self.covfunc.grad_kernel(self.data_x, self.data_x)
        dlnLds_f = np.linalg.multi_dot(
            [self.YT, self.invcov_KC, grad_kernel[0], self.invcov_KC, self.Y]
        ) - np.trace(self.invcov_KC.dot(grad_kernel[0]))

        dlnLdl = np.linalg.multi_dot(
            [self.YT, self.invcov_KC, grad_kernel[1], self.invcov_KC, self.Y]
        ) - np.trace(self.invcov_KC.dot(grad_kernel[1]))
        return -.5 * np.array([dlnLds_f, dlnLdl])

    # ### Forward KL divergence between the reconstruction and the data
    # def KL_divergence_forward(self, target):
    #     dy = np.block(target["y"]) - self.predict_mean(target["x"])
    #     cov_gp = self.predict_cov(target["x"])
    #
    #     res = np.linalg.multi_dot([dy.T, target["invcov"], dy])
    #     res -= np.linalg.slogdet(cov_gp)[1] - np.linalg.slogdet(target["cov"])[1]
    #     res += np.trace(np.dot(target["invcov"], cov_gp))
    #     res -= float(len(dy))
    #
    #     return 0.5 * res
    #
    #
    # ### Reverse KL divergence between the reconstruction and the data
    # def KL_divergence_reverse(self, target):
    #     dy = np.block(target["y"]) - self.predict_mean(target["x"])
    #     cov_gp = self.predict_cov(target["x"])
    #     invcov_gp = utils.QR_inverse(cov_gp)
    #
    #     res = np.linalg.multi_dot([dy.T, invcov_gp, dy])
    #     res -= np.linalg.slogdet(target["cov"])[1] + np.linalg.slogdet(cov_gp)[1]
    #     res += np.trace(np.dot(target["cov"], invcov_gp))
    #     res -= float(len(dy))
    #     return 0.5 * res
