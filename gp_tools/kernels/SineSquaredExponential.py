from __future__ import absolute_import
import sys
import numpy as np


class Covfunc:
    def __init__(self, args_cov=None):
        self.name = "SineSquaredExponential"
        self.abrv = "SinE"
        self.args_cov = args_cov

        if args_cov == "novar":
            self.n_hyp = 2
            self.default_hyp_values = [0.1, 0.1]
        else:
            self.n_hyp = 3
            self.default_hyp_values = [0.1, 0.1, 0.1]

    # hyper parameter values setter
    def set_hyp_values(self, p):
        if self.args_cov == "novar":
            self.sigma = 1
            self.l = p[0]
            self.theta = p[1]
        else:
            self.sigma = p[0]
            self.l = p[1]
            self.theta = p[2]

    # the kernel
    def kernel(self, X, Y):
        return self.sigma ** 2 * np.exp(
            -2
            / self.l ** 2
            * (np.sin(np.pi / self.theta * np.subtract.outer(X, Y))) ** 2
        )
