import numpy as np

from .. import load_covfunc


class Covfunc:
    def __init__(self, dic):

        self.dic = dic
        self.names = dic["name"]
        self.shape = np.shape(self.names)

        multicov = []
        for i in range(self.shape[0]):
            for j in range(self.shape[1]):
                if self.names[i][j] == "None":
                    multicov.append("None")

                elif self.names[i][j] == "Conv":
                    multicov.append("Conv")

                else:

                    covariance_function = self.dic.copy()
                    covariance_function["name"] = self.names[i][j]
                    multicov.append(load_covfunc.Load(
                        covariance_function).covfunc)

        self.multicov = np.reshape(multicov, self.shape)

    # Setter of the hyper parameters
    def set_hyp_values(self, p):
        ct = 0
        for i in range(self.shape[0]):
            for j in range(i, self.shape[1]):
                if not self.names[i][j] == "None" and not self.names[i][j] == "Conv":
                    n_hyp = self.multicov[i][j].n_hyp
                    self.multicov[i][j].set_hyp_values(p[ct: ct + n_hyp])

                    if i != j:
                        self.multicov[j][i].set_hyp_values(p[ct: ct + n_hyp])

                    ct += n_hyp

    # the kernel
    def kernel(self, X, Y):
        kernel = np.empty(self.shape).tolist()
        for i in range(self.shape[0]):
            for j in range(self.shape[1]):
                if self.names[i][j] == "None":
                    kernel[i][j] = np.zeros([len(X[i]), len(Y[j])])

                elif self.names[i][j] == "Conv":

                    hyp_f = [self.multicov[i][i].sigma, self.multicov[i][i].l]
                    hyp_sig = [self.multicov[j]
                               [j].sigma, self.multicov[j][j].l]
                    r = np.subtract.outer(X[i], Y[j])

                    kernel[i][j] = (
                        hyp_f[0]
                        * hyp_sig[0]
                        * np.sqrt(
                            (2 * hyp_f[1] * hyp_sig[1])
                            / (hyp_f[1] ** 2 + hyp_sig[1] ** 2)
                        )
                        * np.exp(-(r ** 2) / (hyp_f[1] ** 2 + hyp_sig[1] ** 2))
                    )

                else:
                    kernel[i][j] = self.multicov[i][j].kernel(X[i], Y[j])

        return np.block(kernel)
