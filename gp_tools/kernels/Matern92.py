import numpy as np


class Covfunc:
    def __init__(self):
        self.name = "Matern92"
        self.abrv = "Mat92"
        self.n_hyp = 2

    # hyper parameter values setter
    def set_hyp_values(self, p):
        self.sigma, self.l = p

    # the kernel
    def kernel(self, X, Y):
        rl = np.abs(np.subtract.outer(X, Y)) / self.l
        return (
            self.sigma ** 2
            * (
                1
                + 3.0 * rl
                + 27.0 / 7.0 * rl ** 2
                + 18.0 / 7.0 * rl ** 3
                + 27.0 / 35.0 * rl ** 4
            )
            * np.exp(-3.0 * rl)
        )
