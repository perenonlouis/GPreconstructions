from __future__ import absolute_import
import sys
import numpy as np


class Covfunc:
    def __init__(self):
        self.name = "RationalQuadratic"
        self.abrv = "RQ"
        self.n_hyp = 3

    # hyper parameter values setter
    def set_hyp_values(self, p):
        self.sigma, self.l, self.alpha = p

    # the kernel
    def kernel(self, X, Y):
        r = np.abs(np.subtract.outer(X, Y))
        return self.sigma ** 2 * (1 + r ** 2 / (2 * self.alpha * self.l ** 2)) ** (
            -self.alpha
        )
