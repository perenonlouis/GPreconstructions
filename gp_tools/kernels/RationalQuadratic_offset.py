from __future__ import absolute_import
import sys
import numpy as np


class Covfunc:
    def __init__(self, args_cov=None):
        self.name = "RationalQuadratic_offset"
        self.abrv = "RQ"
        self.args_cov = args_cov

        if args_cov == "novar":
            self.n_hyp = 3
            self.default_hyp_values = [0.1, 0.1, 0.1]
            self.default_hyp_priors = [[0.0, 100.0], [0.0, 100.0], [0.0, 100.0]]

        else:
            self.n_hyp = 4
            self.default_hyp_values = [0.1, 0.1, 0.1, 0.1]
            self.default_hyp_priors = [
                [0.0, 100.0],
                [0.0, 100.0],
                [0.0, 100.0],
                [0.0, 100.0],
            ]

    # hyper parameter values setter
    def set_hyp_values(self, p):
        if self.args_cov == "novar":
            self.sigma = 1
            self.l = p[0]
            self.alpha = p[1]
            self.offset = p[2]

        else:
            self.sigma = p[0]
            self.l = p[1]
            self.alpha = p[2]
            self.offset = p[3]

    # the kernel
    def kernel(self, X, Y):
        r = np.abs(np.subtract.outer(X, Y))
        return (
            self.sigma ** 2
            * (1 + r ** 2 / (2 * self.alpha * self.l ** 2)) ** (-self.alpha)
            + self.offset
        )
