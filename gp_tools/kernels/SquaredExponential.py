from __future__ import absolute_import
import sys
import numpy as np


class Covfunc:
    def __init__(self):
        self.name = "SquaredExponential"
        self.abrv = "SE"
        self.n_hyp = 2

    # hyper parameter values setter
    def set_hyp_values(self, p):
        self.sigma, self.l = p

    # the kernel
    def kernel(self, X, Y):
        return self.sigma ** 2 * np.exp(-0.5 * (np.subtract.outer(X, Y) / self.l) ** 2)

    # the gradient of the kernel d2K/(dsigma dl)
    def grad_kernel(self, X, Y):
        dist = (np.subtract.outer(X, Y) / self.l) ** 2
        dk_dsigma = 2 * self.sigma * np.exp(-0.5 * dist)
        dk_dl = self.sigma ** 2 / self.l * dist * np.exp(-0.5 * dist)
        return np.array([dk_dsigma, dk_dl])

    # the derivative of the kernel dK/dY
    def d_kernel(self, X, Y):
        sdist = -np.subtract.outer(X, Y)
        return (self.sigma / self.l) ** 2 * np.exp(-0.5 * (sdist / self.l) ** 2) * sdist

    # the derivative of the kernel d2K/(dX dY)
    def dd_kernel(self, X, Y):
        dist = (np.subtract.outer(X, Y) / self.l) ** 2
        return (self.sigma / self.l) ** 2 * np.exp(-0.5 * dist) * (1 - dist)

    # the derivative of the kernel d4K/(d2X d2Y)
    def d2d2_kernel(self, X, Y):
        dist = (np.subtract.outer(X, Y) / self.l) ** 2
        return (
            self.sigma ** 2
            / self.l ** 4
            * np.exp(-0.5 * dist)
            * (3.0 - 6 * dist + dist ** 2)
        )
