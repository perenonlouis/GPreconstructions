import numpy as np


class Covfunc:
    def __init__(self, args_cov=None):
        self.name = "Constant"
        self.abrv = "Cst"
        self.args_cov = args_cov
        self.n_hyp = 1
        self.default_hyp_values = [0.1]

    # hyper parameter values setter
    def set_hyp_values(self, p):
        self.sigma = p[0]

    # the kernel
    def kernel(self, X, Y):
        return np.full((len(X), len(Y)), self.sigma * 2)
