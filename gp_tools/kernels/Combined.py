import numpy as np
import scipy.signal

from . import mod_GP as GP


class Covfunc:
    def __init__(self, names, args_cov=None):

        self.names = names
        self.dim = len(names)
        self.args_cov = args_cov

        self.multicov = []
        self.default_hyp_values = []

        for name in self.names:
            ins = GP.set_covfunc(name)
            self.multicov.append(ins)
            self.default_hyp_values.append(ins.default_hyp_values)

    # Setter of the hyper parameters
    def set_hyp_values(self, p):
        ct = 0
        for i in range(self.dim):
            n_hyp = self.multicov[i].n_hyp
            self.multicov[i].set_hyp_values(p[ct : ct + n_hyp])
            ct += n_hyp

    # the kernel
    def kernel(self, X, Y):
        mat = self.multicov[0].kernel(X, Y)
        for i, operation in enumerate(self.args_cov):
            if operation == "+":
                mat += self.multicov[i].kernel(X, Y)
            if operation == "*":
                mat *= self.multicov[i].kernel(X, Y)

        return mat
