import numpy as np


class Covfunc:
    def __init__(self):
        self.name = "Matern52"
        self.abrv = "Mat52"
        self.n_hyp = 2

    # hyper parameter values setter
    def set_hyp_values(self, p):
        self.sigma, self.l = p

    # the kernel
    def kernel(self, X, Y):
        rl = np.abs(np.subtract.outer(X, Y)) / self.l
        return (
            self.sigma ** 2
            * (1.0 + np.sqrt(5.0) * rl + 5.0 / 3.0 * rl ** 2)
            * np.exp(-np.sqrt(5.0) * rl)
        )
