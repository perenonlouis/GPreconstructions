import numpy as np


class Covfunc:
    def __init__(self, args_cov=None):
        self.name = "Matern32_offset"
        self.abrv = "Mat32_offset"
        self.args_cov = args_cov

        if args_cov == "novar":
            self.n_hyp = 2
            self.default_hyp_values = [0.1, 0.1]
            self.default_hyp_priors = [[0.0, 10.0], [0.0, 10.0], [0.0, 10.0]]

        else:
            self.n_hyp = 3
            self.default_hyp_values = [0.1, 0.1, 0.1]
            self.default_hyp_priors = [[0.0, 10.0], [0.0, 10.0], [0.0, 10.0]]

    # hyper parameter values setter
    def set_hyp_values(self, p):
        if self.args_cov == "novar":
            self.sigma = 1
            self.l = p[0]
            self.offset = p[1]

        else:
            self.sigma = p[0]
            self.l = p[1]
            self.offset = p[2]

    # the kernel
    def kernel(self, X, Y):
        rl = np.abs(np.subtract.outer(X, Y)) / self.l
        return (
            self.sigma ** 2 * (1.0 + np.sqrt(3.0) * rl) * np.exp(-np.sqrt(3.0) * rl)
            + self.offset
        )
