import numpy as np


class Covfunc:
    def __init__(self):
        self.name = "Matern72"
        self.abrv = "Mat72"
        self.n_hyp = 2

    # hyper parameter values setter
    def set_hyp_values(self, p):
        self.sigma, self.l = p

    # the kernel
    def kernel(self, X, Y):
        rl = np.abs(np.subtract.outer(X, Y)) / self.l
        return (
            self.sigma ** 2
            * (
                1
                + np.sqrt(7.0) * rl
                + 14.0 / 5.0 * rl ** 2
                + 7 * np.sqrt(7.0) / 15.0 * rl ** 3
            )
            * np.exp(-np.sqrt(7.0) * rl)
        )
