import numpy as np


class Covfunc:
    def __init__(self):
        self.name = "Exponential"
        self.abrv = "E"
        self.n_hyp = 2

    # hyper parameter values setter
    def set_hyp_values(self, p):
        self.sigma, self.l = p

    # the kernel
    def kernel(self, X, Y):
        return self.sigma ** 2 * np.exp(-np.abs(np.subtract.outer(X, Y)) / self.l)
