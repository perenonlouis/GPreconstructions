import numpy as np


class Covfunc:
    def __init__(self, args_cov=None):
        self.name = "Polynomial"
        self.abrv = "Poly"
        self.args_cov = args_cov

        self.n_hyp = 1
        self.default_hyp_values = [0.1]

    # hyper parameter values setter
    def set_hyp_values(self, p):
        self.offset = p[0]

    # the kernel
    def kernel(self, X, Y):
        return self.offset + np.dot(X, Y)
