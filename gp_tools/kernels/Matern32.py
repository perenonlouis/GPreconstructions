import numpy as np


class covfunc:
    def __init__(self):
        self.name = "Matern32"
        self.abrv = "Mat32"
        self.n_hyp = 2

    # hyper parameter values setter
    def set_hyp_values(self, p):
        self.sigma, self.l = p

    # the kernel
    def kernel(self, X, Y):
        rl = np.abs(np.subtract.outer(X, Y)) / self.l
        return self.sigma ** 2 * (1.0 + np.sqrt(3.0) * rl) * np.exp(-np.sqrt(3.0) * rl)
