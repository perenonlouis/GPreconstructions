import numpy as np


class Load:
    """This class loads the covariance function dictionary into the class
    variables
    """

    def __init__(self, covariance_function):

        self.covariance_function = covariance_function

        if isinstance(covariance_function["name"], str):

            if covariance_function["name"] in ("Constant", "Cst"):
                from .kernels import Constant as kernel

            if covariance_function["name"] in ("WhiteNoise", "WN"):
                from .kernels import WhiteNoise as kernel

            if covariance_function["name"] in ("Exponential", "E"):
                from .kernels import Exponential as kernel

            if covariance_function["name"] in ("SquaredExponential", "SE"):
                from .kernels import SquaredExponential as kernel

            if covariance_function["name"] in ("GammaExponential", "GE"):
                from .kernels import GammaExponential as kernel

            if covariance_function["name"] in ("RationalQuadratic", "RQ"):
                from .kernels import RationalQuadratic as kernel

            if covariance_function["name"] in ("RationalQuadratic_offset", "RQ_offset"):
                from .kernels import RationalQuadratic_offset as kernel

            if covariance_function["name"] in ("Matern32", "Mat32"):
                from .kernels import Matern32 as kernel

            if covariance_function["name"] in ("Matern32_offset", "Mat32_offset"):
                from .kernels import Matern32_offset as kernel

            if covariance_function["name"] in ("Matern52", "Mat52"):
                from .kernels import Matern52 as kernel

            if covariance_function["name"] in ("Matern72", "Mat72"):
                from .kernels import Matern72 as kernel

            if covariance_function["name"] in ("Matern92", "Mat92"):
                from .kernels import Matern92 as kernel

            if covariance_function["name"] in ("Polynomial", "Poly"):
                from .kernels import Polynomial as kernel

            if covariance_function["name"] in ("SineSquaredExponential", "SinE"):
                from .kernels import SineSquaredExponential as kernel

            self.covfunc = kernel.Covfunc()

        else:
            if np.shape(covariance_function["name"])[1] > 1:
                from .kernels import Multi as kernel

            elif np.shape(covariance_function["name"])[0] > 1:
                from .kernels import Combined as kernel

            self.covfunc = kernel.Covfunc(covariance_function)

        self.default_hyp_values = covariance_function["hyp_values"]

    # hyper parameter values setter

    def set_hyp_values(self, p):
        return self.covfunc.set_hyp_values(p)

    # The kernel
    def kernel(self, X, Y):
        return self.covfunc.kernel(X, Y)

    # The gradient of the kernel d2K/(dsigma dl)
    def grad_kernel(self, X, Y):
        return self.covfunc.grad_kernel(X, Y)

    # The derivative of the kernel dK/dY
    def d_kernel(self, X, Y):
        return self.covfunc.d_kernel(X, Y)

    # The derivative of the kernel d2K/(dX dY)
    def dd_kernel(self, X, Y):
        return self.covfunc.ds_kernel(X, Y)

    # The derivative of the kernel d4K/(d2X d2Y)
    def d2d2_kernel(self, X, Y):
        return self.covfunc.d2d2_kernel(X, Y)
