import numpy as np
import pandas as pd

from getdist.mcsamples import MCSamples

import config
from gp_tools import gp as GP
from sampling_tools import sampler
from sampling_tools import minimizer
from plotting_tools.plotter import plot_contours
from plotting_tools.plotter import plot_reconstruction


####################
### Computations ###
####################


class MainEngine:
    """ ... """  # docstring has to be written in the future

    def __init__(
        self,
        general_options,
        data,
        GPinfo,
        options=None,
        runall=True,
        fiducial=None
    ):


        # --- Setting the minimum attributes required for this class
        self.fiducial = fiducial

        # The configuration module sets the defaults
        config.set_general_options(general_options,data,GPinfo)
        config.set_defaults()

        self.covfunc       = config.set_covfunc(config,GPinfo)
        self.sampling_info = config.set_sampling(config,GPinfo)

        # --- Run all the class method... (LP: better comment needed) We shoudl probaly create skips kew words so as to turn off what we want

        # --- Progress checkers
        # MM: moved them here as they should be set to False before they are actually touched in the functions
        # Anyway, I would remove this. Checks should be internal to the module.
        # At the moment if someone wants to feed their own gp instance to the sampler, the code would fail unless
        # they manually set to True the self.done_gp variable.
        self.done_gp = False
        self.done_sampling = False
        self.done_mcmc = False
        self.done_bestfit = False
        self.done_marginalise = False
        self.postprocessing = False

        bestsamp = None

        if runall: self.fullrun(data, self.covfunc, self.sampling_info)



    def fullrun(self, data, covfunc, sampling_info):

        self.best_hyperpars = {}
        self.reconstruction = {}
        if config.specs['method'] == 'single task':
            self.gpresults      = {}
            for ind,obs in enumerate(config.specs['observables']):

                print('')
                print('##########################')
                print('Doing single task on '+obs)
                print('##########################')
                print('')

                singledata = {'x': data['x'][ind],
                              'y': data['y'][ind],
                              'cov': np.diag(data['error'][ind]**2.)}

                gp = self.set_gp(singledata, covfunc[ind])
                self.gpresults[obs] = gp

                if config.options['minimize_hyperparameters']:
                   bestsamp, self.best_hyperpars[obs] = self.minimize(gp, sampling_info[ind])

                if config.options['sample_hyperparameters']:

                   sampling_results = self.do_sampling(gp, sampling_info[ind])
                   plot_contours(sampling_results, bestsamp)

                if config.options['method'] == 'optimization':
                   self.reconstruction[obs] = self.get_opt_reconstruction(gp, self.best_hyperpars[obs])

                elif config.options['method'] == 'marginalisation':
                   self.reconstruction[obs] = self.run_marginalisation(
                        gp, sampling_results)
                else:
                   raise ValueError(
                        "/!\ Unknown method {}. /!\ ".format(config.options['method']))

                self.reconstruction[obs]['xlabel'] = config.specs['xlabels'][ind]
                self.reconstruction[obs]['ylabel'] = config.specs['ylabels'][ind]

            plot_reconstruction(self.gpresults, self.reconstruction, self.fiducial)

        elif config.specs['method'] == 'multi task':

            print('')
            print('##########################')
            print('Doing multi task on {}'.format(config.specs['observables']))
            print('##########################')
            print('')

            gp = self.set_gp(data, covfunc)


            if config.options['minimize_hyperparameters']:
               bestsamp, bestfit_pars = self.minimize(gp, sampling_info)

               for obs in config.specs['observables']:
                   self.best_hyperpars[obs] = {}
                   for key in bestfit_pars:
                       if key.endswith(obs):
                           self.best_hyperpars[obs].update({key: bestfit_pars[key]})


            if config.options['sample_hyperparameters']:

               sampling_results = self.do_sampling(gp, sampling_info)
               plot_contours(sampling_results, bestsamp)

            if config.options['method'] == 'optimization':
               self.reconstruction = self.get_opt_reconstruction(gp, bestfit_pars)

            elif config.options['method'] == 'marginalisation':
               self.reconstruction = self.run_marginalisation(gp, sampling_results)

            else:
               raise ValueError(
                        "/!\ Unknown method {}. /!\ ".format(config.options['method']))

            for ind,obs in enumerate(config.specs['observables']):
                self.reconstruction[obs]['xlabel'] = config.specs['xlabels'][ind]
                self.reconstruction[obs]['ylabel'] = config.specs['ylabels'][ind]

            plot_reconstruction(gp, self.reconstruction, self.fiducial)



    ###############
    ### Setters ###
    ###############

    def set_gp(self, data, covfunc):
        """This function sets the gp instance"""

        gp = GP.Engine(data, covfunc)
        self.done_gp = True

        return gp

    def do_sampling(self, gp, sampling_info):

        if not self.done_gp:
            raise ValueError("/!\ Fool! Set the gp first. /!\ ")
        else:
            results = sampler.Engine(gp, sampling_info)

        return results

    def minimize(self, gp, sampling_info):

        if not self.done_gp:
            raise ValueError("/!\ Fool! Set the gp first. /!\ ")
        else:
            results = minimizer.Engine(gp, sampling_info)

        print('')
        print('########################')
        print('#   Best fit results   #')
        print('########################')

        for key in results.bestfit:
            print(key + '= {:.4f}'.format(results.bestfit[key]))

        print('########################')
        print('########################')
        print('')

        return results.bestsamp, results.bestfit

    def get_opt_reconstruction(self, gp, optpoint):

        if config.specs['method'] == 'single task':

            if 'recon_x' not in config.options.keys():
                x = np.linspace(min(gp.data_x), max(gp.data_x), 500)
            else:
                x = config.options['recon_x']

            bestfit = [optpoint[key] for key in optpoint]
            gp.set_hyp_values(bestfit)
            mean, cov = gp.predict(x, predict_cov=True)
            var = [cov[ind, ind] for ind in range(len(x))]

            recon_func = {
               "x": x,
               'mean':  mean,
               'down1': mean - np.sqrt(var),
               'up1': mean + np.sqrt(var),
               # MM: this is not really a 2, right?
               'down2': mean - 2 * np.sqrt(var),
               'up2': mean + 2 * np.sqrt(var)
            }  # MM: this is not really a 2, right?

        elif config.specs['method'] == 'multi task':
            if 'recon_x' not in config.options.keys():
               x = [np.linspace(min(gp.data_x), max(gp.data_x), 500)]*len(config.specs['observables'])
            else:
               x = config.options['recon_x']

            bestfit = [optpoint[key] for key in optpoint]
            gp.set_hyp_values(bestfit)
            meanblock, cov = gp.predict(x, predict_cov=True)

            #MM: this is the bit that needs to be updated with the sampling of the reconstructed
            #    function. For the moment I ignore covariances

            recon_func = {}
            pos = 0 #This support variable is ugly
            for ind_obs,obs in enumerate(config.specs['observables']):
                var = [cov[ind_obs*len(x[ind_obs])+ind, ind_obs*len(x[ind_obs])+ind] for ind in range(len(x[ind_obs]))]

                mean = np.array([m for m in meanblock[pos:pos+len(x[ind_obs])]])

                pos += len(x[ind_obs])

                recon_func[obs] = {
                   "x": x[ind_obs],
                   'mean':  mean,
                   'down1': mean - np.sqrt(var),
                   'up1': mean + np.sqrt(var),
                   # MM: this is not really a 2, right?
                   'down2': mean - 2 * np.sqrt(var),
                   'up2': mean + 2 * np.sqrt(var)
                }  # MM: this is not really a 2, right?





        return recon_func

    def run_marginalisation(self, gp, sampling_results):
        # LP : this should either eat the chain or use load_sample automatically
        # I think that is also another reason, beyond versatility for users,
        # for having self.sampling.sampler.load_sample() available easily:
        # can't marginalise by eating the load_gd_sample MCS instance
        # This function is going to be a horrible pain using cobaya because
        # we have to be hable to burn and thin the chain at the level of the
        # .txt file and not MCSamplesFromCobaya(). To ease the computation time,
        # I am used to brun and thin the chain much more for marginalisation
        # than if it was for a contour plot. Good luck. Make me proud :D

        # This dictionary should have different key than the post process one,
        # but maybe the MainEngine could have a post process dicitonnary which
        # would have the keys for the post process and marginalisation.

        # MM: if we get the output of emcee and put this into objects
        # that fit MCSamplesFromCobaya we can use this
        from getdist.mcsamples import MCSamplesFromCobaya
        from time import time

        print("\n >>> Running the marginalisation \n")
        tini = time()

        # MM: options commented out for the moment
        # options_default = {
        #    "from_file": False,
        #    "burn": 0.3,
        #    "thin": 1,
        #    "smooth_scale_1D": 0.4,
        #    "smooth_scale_2D": 0.4,
        # }
        # if options is not None:
        #    options_default.update(options)

        mc_marg = config.defaults["mc_gen_for_marginalisation"]

        if 'recon_x' not in config.options.keys():
            x = np.linspace(min(gp.data_x), max(gp.data_x), 300)
        else:
            x = config.options['recon_x']

        gd_sample = MCSamplesFromCobaya(
            sampling_results.info,
            sampling_results.sampling_results["sample"],
            ignore_rows=config.defaults["chain_burn_for_marginalisation"],
        )
        gd_sample.thin(config.defaults["chain_thin_for_marginalisation"])
        chain = gd_sample.samples[:, :-2]
        leng = len(chain)
        dist = np.empty([len(x), leng * mc_marg])
        dist = np.empty([leng * mc_marg, len(x)])
        tloop = time()
        for ind in range(leng):
            print('  >> Marginalisation step %s of %s' % (ind + 1, leng))
            # print('     > Hyperparameters:', chain[ind, :])
            gp.set_hyp_values(chain[ind, :])
            try:
                mean, cov = gp.predict(x, predict_cov=True)
                rand = np.random.multivariate_normal(mean, cov, size=mc_marg)
                dist[ind * mc_marg:(ind + 1) * mc_marg, :] = rand.squeeze()
            except:
                pass
            # print('     > mean:', mean)
            # print('     > cov:', cov)

            trun = time()
            if (trun - tloop) > 300:
                self.enjoy()
                tloop = trun

        # Getdist to compute the distribution at each redshift
        name_gd = []
        for xi in x:
            name_gd = np.append(name_gd, 'f(' + str(xi) + ')')
        MCS = MCSamples(samples=dist, names=name_gd)
        f_mean = MCS.getMeans()
        f_err_top_68 = []
        f_err_bot_68 = []
        f_err_top_95 = []
        f_err_bot_95 = []
        for i, name in enumerate(name_gd):
            f_err_top_68.append(
                MCS.getMargeStats().parWithName(name).limits[0].upper)
            f_err_bot_68.append(
                MCS.getMargeStats().parWithName(name).limits[0].lower)
            f_err_top_95.append(
                MCS.getMargeStats().parWithName(name).limits[1].upper)
            f_err_bot_95.append(
                MCS.getMargeStats().parWithName(name).limits[1].lower)

        recon_func = {
            "x": x,
            'mean': f_mean,
            'down1': f_err_bot_68,
            'up1': f_err_top_68,
            'down2': f_err_bot_95,
            'up2': f_err_top_95,
        }

        tend = time()
        print("\n <<< The marginalisation is done in {:.2f} s\n".format(
            tend - tini))

        return recon_func

    def enjoy(self):

        print('')
        print('This loop is taking a while...')
        print('enjoy some music in the meantime!')

        import random

        song = random.randint(1, 5)

        if song == 1:
            link = 'https://www.youtube.com/watch?v=6axOY4PBusk'
        if song == 2:
            link = 'https://www.youtube.com/watch?v=PjKtQO3IPrk'
        if song == 3:
            link = 'https://www.youtube.com/watch?v=wtESlTKBa4s'
        if song == 4:
            link = 'https://www.youtube.com/watch?v=mOYZaiDZ7BM'
        if song == 5:
            link = 'https://www.youtube.com/watch?v=YZLnGNrsCYg'

        print('<a href="{0}">{0}</a>'.format(link))
