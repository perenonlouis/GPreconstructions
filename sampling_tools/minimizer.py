import numpy as np
import config

from .load_likelihoods import Load as load_likelihoods

from cobaya.run import run

"""This is the sampling package for the GP's
"""


class Engine:
    """This class is for the sampling of a GP

    Attributes:
        name (str): the name for the files
        gp (inst): the gp instance dictionary
        sampling_info (dic): the sampling information dictionary
    """

    def __init__(self, gp, sampling_info):
        """The constructor for Engine class

        Attributes:
            name (str): the name for the files
            gp (inst): the gp instance dictionary
            sampling_info (dic): the sampling information dictionary
        """

        # --- Setting the attributes
        if "sampling_func" not in sampling_info:
            sampling_info["sampling_func"] = lambda x: x

        like = load_likelihoods(sampling_info, gp)

        if 'cobaya' in sampling_info['minimizer']:
            self.bestsamp, self.bestfit = self.cobaya_minimize(
                gp, sampling_info, like)
        elif 'emcee' in sampling_info['minimizer']:
            raise ValueError(
                "/!\ emcee minimizer coming soon, be patient /!\ ")
        else:
            raise ValueError(
                "/!\ Unknown sampler {}. /!\ ".format(sampling_info['sampler']))

    def cobaya_minimize(self, gp, sampling_info, like):

        # Feeding the parameters to cobaya
        params = {}
        for i, par in enumerate(like.gp.covariance_function["hyp_names"]):
            if sampling_info["hyp_ranges"][par][0] == sampling_info["hyp_ranges"][par][1]:
                params[par] = sampling_info["hyp_ranges"][par][0]
            else:
                params[par] = {'prior': {'min': sampling_info["hyp_ranges"][par][0],
                                         'max': sampling_info["hyp_ranges"][par][1]},
                               'latex': sampling_info["hyp_labels"][i]}

        cobaya_info = {
            "params": params,
            'likelihood': {"cobaya": {"external": like.cobaya_loglike}},
            'force': True
        }

        if config.options['print_chains']:
            cobaya_info['output'] = config.options['name_run']

        if sampling_info['minimizer'] == 'cobaya_bobyqa':
            cobaya_info['sampler'] = {"minimize": {'method': 'bobyqa'}}

        elif sampling_info['minimizer'] == 'cobaya_scipy':
            cobaya_info['sampler'] = {"minimize": {'method': 'scipy'}}

        else:
            raise ValueError(
                "/!\ Unknown Cobaya minimizer {} /!\ ".format(sampling_info['minimizer']))

        #from config import HiddenPrints
        #with HiddenPrints():
        update_info, sample = run(cobaya_info)

        minvec = (np.dot(sample.products()['M'], sample.products()['result_object'].x) +
                  sample.products()['X0'])

        bestfit = {}
        bestsamp = {}
        for i, par in enumerate(like.gp.covariance_function["hyp_names"]):
            bestsamp[par] = minvec[i]
            bestfit[par] = sampling_info['sampling_func'][i](minvec[i])

        return bestsamp, bestfit
