import numpy as np
import config


class Load:
    def __init__(self, sampling_info, gp):

        # --- Setting the likelihood
        if sampling_info["likelihood"] == "logmarginal":
            from .likelihoods import logmarginal as like

        self.gp = gp
        self.loglike = like.LogLikelihood(gp)
        self.hyp_ranges = np.array(
            [sampling_info["hyp_ranges"][par][:] for par in sampling_info["hyp_ranges"]])

        # --- Setting default bad_res for the likelihood to return
        self.bad_res = np.inf  # overwritten when Sampler() instanciated

        # --- Setting the type of parameter sampling
        self.sampling_func = sampling_info["sampling_func"][0] #MM: THIS NEEDS TO BE FIXED!


    def compute(self, p):
        if not np.all((self.hyp_ranges[:, 0] <= p)
                      & (p <= self.hyp_ranges[:, 1])):
            return self.bad_res

        pars = self.sampling_func(p)

        try:
            res = self.loglike.value(pars)
        except:
            return self.bad_res

        if np.isnan(res):
            return self.bad_res

        return res

    # MM: warning, this needs to be improved and made generic

    def cobaya_loglike(
        self, sigma=1,length=1,
        sigma_f=1, length_f=1,
        sigma_sigma8=1, length_sigma8=1,
        sigma_fsigma8=1, length_fsigma8=1
    ):

        if isinstance(self.gp.covariance_function["name"],str):
            if self.gp.covariance_function["name"] in ("RQ",):
                value = self.compute([sigma, length, alpha])
            else:
                value = self.compute([sigma, length])

        else:
            if len(self.gp.covariance_function["name"]) > 2:
                value = self.compute([
                    sigma_f, length_f,
                    sigma_sigma8, length_sigma8,
                    sigma_fsigma8, length_fsigma8,
                ])




        return value
