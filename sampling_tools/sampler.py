import numpy as np
import config

from .load_likelihoods import Load as load_likelihoods

from cobaya.run import run

"""This is the sampling package for the GP's
"""


class Engine:
    """This class is for the sampling of a GP

    Attributes:
        name (str): the name for the files
        gp (inst): the gp instance dictionary
        sampling_info (dic): the sampling information dictionary
    """

    def __init__(self, gp, sampling_info):
        """The constructor for Engine class

        Attributes:
            name (str): the name for the files
            gp (inst): the gp instance dictionary
            sampling_info (dic): the sampling information dictionary
        """

        # --- Setting the attributes
        if "sampling_func" not in sampling_info:
            sampling_info["sampling_func"] = lambda x: x

        like = load_likelihoods(sampling_info, gp)

        print(sampling_info['sampler'])
        if 'cobaya' in sampling_info['sampler']:
            self.info, self.sampling_results = self.cobaya_sampling(
                gp, sampling_info, like)
        elif 'emcee' in sampling_info['sampler']:
            raise ValueError("/!\ emcee sampling coming soon, be patient /!\ ")
        else:
            raise ValueError(
                "/!\ Unknown sampler {}. /!\ ".format(sampling_info['sampler']))

    def cobaya_sampling(self, gp, sampling_info, like):

        # Feeding the parameters to cobaya
        params = {}
        for i, par in enumerate(like.gp.covariance_function["hyp_names"]):
            if sampling_info["hyp_ranges"][par][0] == sampling_info["hyp_ranges"][par][1]:
                params[par] = sampling_info["hyp_ranges"][par][0]
            else:
                params[par] = {'prior': {'min': sampling_info["hyp_ranges"][par][0],
                                         'max': sampling_info["hyp_ranges"][par][1]},
                               'latex': sampling_info["hyp_labels"][i]}

        cobaya_info = {
            "params": params,
            'likelihood': {"cobaya": {"external": like.cobaya_loglike}},
            'force': True
        }

        if config.options['print_chains']:
            cobaya_info['output'] = config.options['name_run']

        if sampling_info['sampler'] == 'cobaya_MH':
            cobaya_info['sampler'] = {
                "mcmc": {
                    "max_tries": config.defaults["cobaya_MH_max_tries"],
                    "Rminus1_stop": config.defaults["cobaya_MH_Rminus1_stop"]
                }
            }

            config.options['burn_in'] = config.defaults["cobaya_MH_chain_burn"]

            if "max_tries" in sampling_info.keys():
                cobaya_info["sampler"]['mcmc']["max_tries"] = \
                    sampling_info["max_tries"]

            if "Rminus1_stop" in sampling_info.keys():
                cobaya_info["sampler"]['mcmc']["Rminus1_stop"] = \
                    sampling_info["Rminus1_stop"]

        elif sampling_info['sampler'] == 'cobaya_polychord':
            cobaya_info['sampler'] = {
                'polychord': {
                    'nlive': config.defaults["cobaya_polychord_nlive"],
                }
            }
            cobaya_info['packages_path'] = config.options['path_to_Polychord']

            config.options['burn_in'] = 0

            if "nlive" in sampling_info.keys():
                cobaya_info["sampler"]['polychord']["nlive"] = \
                    sampling_info["nlive"]

        else:
            raise ValueError(
                "/!\ Unknown Cobaya sampler {} /!\ ".format(sampling_info['sampler']))

        update_info, sample = run(cobaya_info)

        return update_info, sample.products()
